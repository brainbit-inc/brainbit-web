export const DEVICE_INFORMATION_SERVICE = 0x180A;
export const BRAINBIT_SERVICE = '6e400001-b534-f393-68a9-e50e24dcca9e';
export const BLE_UUID_NSS2_STATUS_CHAR = '6e400002-b534-f393-68a9-e50e24dcca9e';
export const BLE_UUID_NSS2_COMMAND_CHAR = '6e400003-b534-f393-68a9-e50e24dcca9e';
export const BLE_UUID_NSS2_SIGNAL_CHAR = '6e400004-b534-f393-68a9-e50e24dcca9e';
export const commands = {
  invalid: 0,
  stop: 1, // stop everything
  signal: [2, 0, 0, 0, 0], // start signal
  resist: {
    ch1: {
      direct: [3, 48, 145, 145, 145, 1, 1, 0],
      reversed: [3, 48, 145, 145, 145, 1, 1, 255],
    },
    ch2: {
      direct: [3, 145, 48, 145, 145, 2, 2, 0],
      reversed: [3, 145, 48, 145, 145, 2, 2, 255],
    },
    ch3: {
      direct: [3, 145, 145, 48, 145, 4, 4, 0],
      reversed: [3, 145, 145, 48, 145, 4, 4, 255],
    },
    ch4: {
      direct: [3, 145, 145, 145, 48, 8, 8, 0],
      reversed: [3, 145, 145, 145, 48, 8, 8, 255],
    },
  },
  bootloader: [3],
};

export const statuses = [
  {
    name: 'NSS2_STATUS_INVALID',
    value: 0,
    message: 'Sevice is not initialized',
  },
  {
    name: 'NSS2_STATUS_STOPED',
    value: 1,
    message: 'Module is stopped',
  },
  {
    name: 'NSS2_STATUS_SIGNAL',
    value: 2,
    message: 'Signal measurement started',
  },
  {
    name: 'NSS2_STATUS_RESIST',
    value: 3,
    message: 'Resistance measurement started',
  },
  {
    name: 'NSS2_STATUS_BOOTLOADER_NEED',
    value: 4,
    message: 'Bootloader need',
  },
];

export const errors = [
  {
    name: 'NSS2_ERROR_NOERROR',
    value: 0,
    message: '',
  },
  {
    name: 'NSS2_ERROR_LEN',
    value: 1,
    message: 'Incorrect command length',
  },
  {
    name: 'NSS2_ERROR_MODE',
    value: 2,
    message: 'Mode error (mode change is not possible)',
  },
];

export const DEVICE_INFO_CHARACTERISTICS_UUIDS_BY_NAME = {
  model: '00002a24-0000-1000-8000-00805f9b34fb',
  hardwareRevision: '00002a27-0000-1000-8000-00805f9b34fb',
  firmwareVersion: '00002a26-0000-1000-8000-00805f9b34fb',
};

export const DEVICE_MODE = {
  SIGNAL: 2,
  RESISTANCE: 3,
};
